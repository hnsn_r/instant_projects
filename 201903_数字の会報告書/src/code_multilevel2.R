# グループレベルの説明変数を含める
M2 <- lmer(log_radon ~ floor + log_uran +
               (1|county), data=radon)
arm::display(M2)


lmer(formula = log_radon ~ floor + log_uran + (1 | county), data = radon)
            coef.est coef.se
(Intercept)  1.47     0.04
floor       -0.67     0.07
log_uran     0.72     0.09

Error terms:
 Groups   Name        Std.Dev.
 county   (Intercept) 0.16
 Residual             0.76
---
number of obs: 919, groups: county, 85
AIC = 2144.2, DIC = 2111.4
deviance = 2122.8
