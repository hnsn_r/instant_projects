library(tidyverse)

#データ読み込み
wells <- read.table("directory_name/Book_Codes/Ch.5/wells.dat")

#近くの安全な井戸までの距離を分かりやすく(100メートル単位に変える)
wells <- wells %>%
    mutate(dist100 = dist/100)
