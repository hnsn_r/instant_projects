fit1 <- glm(switch ~ dist100, data=wells, family=binomial(link="logit"))
arm::display(fit1)

glm(formula = switch ~ dist100, family = binomial(link = "logit"),
    data = wells)
            coef.est coef.se
(Intercept)  0.61     0.06
dist100     -0.62     0.10
---
  n = 3020, k = 2
  residual deviance = 4076.2, null deviance = 4118.1 (difference = 41.9)
