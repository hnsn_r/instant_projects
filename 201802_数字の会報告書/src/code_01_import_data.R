#環境準備
library(tidyverse)

#日本語設定(ggplot2)
theme_set(theme_gray(base_size = 10, base_family = "HiraKakuProN-W3"))

org_data <- read_csv("~/Dropbox/06_院生共同研究会/数字の会/野球選手データ/longdata.csv")

#データの抽出
data <- org_data %>%
    filter(year == 2016,
           !is.na(team))