library(ineq)
#チーム単位のデータの作成
team_summary <- data %>%
    group_by(team) %>%
    summarise(n = n(),
              total_nenpou = 0.0001*sum(nenpou, na.rm=TRUE),
              gini = ineq(nenpou)) %>%
    left_join(ans, by="team")

#個別に図を作成
p1 <- ggplot(team_summary, aes(x=total_nenpou, y=estimate)) +
    geom_smooth(method = "lm", colour="black") +
    geom_point() +
    theme_bw(base_size=14, base_family = "HiraKakuProN-W3") +
    geom_label(aes(label=team), family = "HiraKakuPro-W3") +
    labs(x="チームの年俸合計(億円)", y="本塁打の回帰係数(万円)")
print(p1)

p2 <- ggplot(team_summary, aes(x=gini, y=estimate)) +
    geom_smooth(method = "lm", colour="black") +
    geom_point() +
    theme_bw(base_size=14, base_family = "HiraKakuProN-W3") +
    geom_label(aes(label=team), family = "HiraKakuProN-W3") +
    labs(x="チーム内の年俸のジニ係数", y="本塁打の回帰係数(万円)")
print(p2)

#二つの図を並べて一つの図として作成
library(ggpubr)
figs <- ggarrange(p1, p2,
                  ncol = 2)