---
title: "17年度ポスター用分析"
author: "西野勇人"
date: "`r format(Sys.time(), '%Y/%m/%d')`"
output:
  html_document:
    self_contained: yes
    toc: true
    #toc_float: true #横に目次バーを表示
    number_sections: yes
    code_folding: hide #コードを折りたたむ
    pandoc_args: [
      "--from", "markdown+autolink_bare_uris+tex_math_single_backslash-implicit_figures"
      ]#この1行で、日本語のリンクが機能するようになる
#bibliography: ~/Dropbox/References/00Bibliography.bib
---
<body class="no-thank-yu">

#今回の目的と前提

##前準備
```{r warning=FALSE, message=FALSE}
#環境準備
#library(ineq)
library(readr)
library(ggplot2)
library(dplyr)
library(tidyr)
library(broom)
library(purrr)
library(lme4)
library(RColorBrewer)
#ggplot2の日本語設定
theme_set(theme_gray(base_size = 10, base_family = "HiraKakuProN-W3"))

setwd("~/Dropbox/00_00Project/ritsstat")

org_data <- read_csv("~/Dropbox/06_院生共同研究会/数字の会/野球選手データ/longdata.csv")
```

readrパッケージを使うが、

##データソース(リンク)
- プロ野球データfreakからデータを統合

# 今回行うこと

- チームごとに回帰式
- チームごとの係数とチーム変数で散布図
- マルチレベルモデルの各種図

## 
```{r}
#データの見通し
data <- org_data %>%
  filter(year==2016,
         !is.na(team))
```

# チーム毎に回帰分析を実行

```{r チーム毎に本塁打数と年俸をOLS}
ans <- lm(nenpou ~ honruida + daritsu + nenrei, data=data)
#summary(ans)

ans <- data %>%
  group_by(team) %>%
  do(result = lm(nenpou ~ honruida + daritsu + nenrei, data=.) %>% tidy(.)) %>%
  unnest() %>%
  filter(term=="honruida") %>%
  mutate(upper=estimate + 1.96*std.error,
         lower=estimate - 1.96*std.error)

p <- ggplot(ans, aes(x=reorder(team, estimate), y=estimate, ymin=lower, ymax=upper))
p <- p + theme_gray(base_size=18, base_family = "HiraKakuProN-W3")
p <- p + coord_flip() #たぶん、XとYの反転
p <- p + geom_hline(yintercept = 0, linetype = 'dotted')
p <- p + geom_pointrange(size=1, colour="#154F85")
p <- p + theme(axis.title.y=element_blank())#Yの軸ラベルを非表示
p <- p + theme(axis.title.x=element_blank())#Xの軸ラベルを非表示
#p <- p + scale_fill_brewer(palette="PuBu")
#p <- p + scale_y_continuous(breaks=seq(from=-2.5, to=2.5, by=2)) #軸メモリの設定
print(p)

quartz(file="fig_coef.pdf", type="pdf",  width=8,height=4)
p
dev.off()
```

次に、今推定したチーム毎の回帰係数を年俸と比較してみる。

```{r チームの年俸合計と上記のチーム別回帰係数のプロット}
#チーム単位のデータ
team_2016 <- data %>%
    filter(year==2016) %>%
    group_by(team) %>%
    summarise(n = n(),
              total_nenpou = 0.0001*sum(nenpou, na.rm=TRUE),
              gini = ineq(nenpou)) %>%
    left_join(ans, by="team")

p <- ggplot(team_2016, aes(x=total_nenpou, y=estimate))
p <- p + geom_smooth(method = "lm", colour="#225A8D", fill="#BDC9E1")
p <- p + theme_gray(base_size=14, base_family = "HiraKakuProN-W3")
p <- p + geom_point()
p <- p + geom_label(aes(label=team), family = "HiraKakuPro-W3")
p <- p + labs(x="チームの年俸合計(億円)", y="本塁打の回帰係数(万円)")
p <- p + scale_fill_brewer(palette="PuBu")
print(p)

quartz(file="fig_team_plot.pdf", type="pdf",  width=6,height=5)
p
dev.off()
```

## マルチレベルモデルで推定

```{r}
tmp <- team_2016 %>%
  select(team, total_nenpou, gini) %>%
  mutate(total_nenpou_c = total_nenpou - mean(total_nenpou))
data_nested <- left_join(data, tmp, by="team")

ans <- lmer(nenpou ~ honruida + daritsu + nenrei + total_nenpou_c + honruida*total_nenpou_c + (1 + honruida|team), data=data_nested)
summary(ans)
```

#使わなかったグラフ -- Alluvial diagramで作図

```{r}
data(UCBAdmissions)
tmp <- as.data.frame(UCBAdmissions)

#install.packages("ggalluvial")
library(ggalluvial)

is_alluvial(as.data.frame(UCBAdmissions), logical = FALSE, silent = TRUE)
is_alluvial(tmp)

ggplot(as.data.frame(UCBAdmissions),
       aes(weight = Freq, axis1 = Gender, axis2 = Dept)) +
  geom_alluvium(aes(fill = Admit), width = 1/12) + #流れていく部分を作る
  geom_stratum(width = 1/12, fill = "black", color = "grey") + #棒グラフ部分を作る
  geom_label(stat = "stratum", label.strata = TRUE) +
  scale_x_continuous(breaks = 1:2, labels = c("Gender", "Dept")) +
  ggtitle("UC Berkeley admissions and rejections, by sex and department")
```

## 実際に野球選手でAlluvial Diagramを作成
```{r}
data_alluvium <- org_data %>%
  select(id, team, year) %>%
  spread(year, team) %>%
  rename(t2015 = "2015",
         t2016 = "2016") %>%
  group_by(t2015, t2016) %>%
  summarise(freq = n()) %>%
  filter(!is.na(t2015)|!is.na(t2016))

p <- ggplot(data_alluvium, aes(weight=freq, axis1=t2015, axis2=t2016)) +
  geom_alluvium() +
  geom_stratum() +
  geom_text(stat = "stratum", label.strata = TRUE, family="HiraKakuProN-W3") +
  scale_x_continuous(breaks = 1:2, labels = c("2015", "2016"))

quartz(file="fig_alluvium.pdf", type="pdf",  width=8,height=5)
p
dev.off()
```
